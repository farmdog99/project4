
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * Class that executes parsing and passing of instructions to the skip list
 *
 * @author Alex Tejada
 * @version 1.2
 */
public class CmdParser {
    /**
     * filename to use
     */
    String fileName;
    /**
     * doing this cause I have to
     */
    Skiplist<String, Rectangle> myList;

    /**
     * default constructor
     */
    CmdParser()
    {
        fileName = null;
        myList = new Skiplist<String, Rectangle>();  
    }

    /**
     * Val Constructor
     * @param aFile file to be read in
     */
    CmdParser(String aFile)
    {
        fileName = aFile;
        myList = new Skiplist<String, Rectangle>();
    }

    /**
     * parse a file
     * @param tempInstruction  pass instruction
     * @throws FileNotFoundException throws this
     */
    public void parseFile(Instruction tempInstruction) 
        throws FileNotFoundException
    {
        Scanner sc = new Scanner(new File(fileName));
        String tempLine = null;

        while (sc.hasNextLine())
        {
            tempLine = sc.nextLine().trim();
            tempInstruction = parseLine(tempLine);
            if (!tempInstruction.isValid())
            {
                tempInstruction.printInvalidInstruction();
            }
            else if (tempInstruction.isValid()) //execute a valid instruction
            {
                Rectangle aRect = new Rectangle(tempInstruction.getxCoor(), 
                        tempInstruction.getyCoor(),
                        tempInstruction.getWidth(), 
                        tempInstruction.getHeight());
                KVPair<String, Rectangle> aPair = new 
                        KVPair<String, Rectangle>(tempInstruction.getName(),
                                aRect);

                if (tempInstruction.getCommand().compareTo("insert") == 0)
                {
                    myList.insert(aPair);
                }
                else if (tempInstruction.getCommand().compareTo("removeN") == 0)
                {
                    //call a remove by name
                    myList.removeByName(aPair.theKey);
                }
                else if (tempInstruction.getCommand().compareTo("removeV") == 0)
                {
                    //call a remove by value
                    myList.removeByValue(aPair.theVal);
                }
                else if (tempInstruction.getCommand().compareTo("dump") == 0)
                {
                    //call skiplist dump
                    myList.dump();
                    System.out.println("FreeList Blocks:");
                    System.out.println("(0, 4096)");
                }
                else if (
                        tempInstruction.getCommand().
                        compareTo("regionsearch") == 0)
                {
                    //call a region search
                    KVPair<String, Rectangle>[] sPairArray = 
                            myList.regionSearch(aPair.theVal) ;
                    if (sPairArray[0] == null) {
                    }
                    else {
                        for (int i = 0; i < (sPairArray.length); i++) {
                            if (sPairArray[i] != null)
                                System.out.println("(" + 
                                        sPairArray[i].toString() + ")");
                        }
                    }
                }
                else if (tempInstruction.getCommand().compareTo("search") == 0)
                {
                    //call a search
                    KVPair<String, Rectangle>[] sPairArray = 
                            myList.search(aPair.theKey);
                    if (sPairArray[0] == null) {
                        System.out.println("Rectangle not found: " + 
                                aPair.theKey.toString());
                    }
                    else {
                        for (int i = 0; i < (sPairArray.length); i++) {
                            if (sPairArray[i] != null)
                                System.out.println("(" + 
                                        sPairArray[i].toString() + ")");
                        }
                    }
                }
                else if (tempInstruction.getCommand().
                        compareTo("intersections") == 0)
                {
                    //call an intersections
                    myList.intersections();
                }
            }
        }
        sc.close();
    }

    /**
     * Parse line of the text file and build an instruction out of it.
     * @param aLine a string of commands
     * @return breaks out
     */
    public Instruction parseLine(String aLine)
    {
        String temp = null;
        Instruction tempInstruction = new Instruction();
        Scanner strc = new Scanner(aLine);
        if (strc.hasNext())
        {
            temp =  strc.next().trim();
            if (temp.compareTo("insert") == 0)
            {
                tempInstruction.setCommand(temp);
                tempInstruction.setName(strc.next());
                tempInstruction.setxCoor(strc.nextInt());
                tempInstruction.setyCoor(strc.nextInt());
                tempInstruction.setWidth(strc.nextInt());
                tempInstruction.setHeight(strc.nextInt());
                tempInstruction.validateRectangleInsert();
            }
            else if (temp.compareTo("remove") == 0)
            {
                tempInstruction.setCommand(temp);
                temp = strc.next();
                char tempchar = temp.charAt(0);
                if (Character.isDigit(tempchar) || tempchar == '-') {
                    tempInstruction.setCommand("removeV");
                    int tempInt = Integer.parseInt(temp);
                    tempInstruction.setName(null);
                    tempInstruction.setxCoor(tempInt);
                    tempInstruction.setyCoor(strc.nextInt());
                    tempInstruction.setWidth(strc.nextInt());
                    tempInstruction.setHeight(strc.nextInt());
                    tempInstruction.validateRectangleInsert();
                }
                else {
                    tempInstruction.setCommand("removeN");
                    tempInstruction.setName(temp);
                    tempInstruction.setValid(true);
                }
            }
            else if (temp.compareTo("regionsearch") == 0)
            {
                tempInstruction.setCommand(temp);
                tempInstruction.setxCoor(strc.nextInt());
                tempInstruction.setyCoor(strc.nextInt());
                tempInstruction.setWidth(strc.nextInt());
                tempInstruction.setHeight(strc.nextInt());
                tempInstruction.validateRectangleRegSearch();
            }
            else if (temp.compareTo("intersections") == 0)
            {
                tempInstruction.setCommand(temp);
                tempInstruction.setValid(true);
            }
            else if (temp.compareTo("search") == 0)
            {
                tempInstruction.setCommand(temp);
                tempInstruction.setName(strc.next());
                tempInstruction.setValid(true);
            }
            else if (temp.compareTo("dump") == 0)
            {
                tempInstruction.setCommand(temp);
                tempInstruction.setValid(true);
            }
        }
        strc.close();
        return tempInstruction;
    }
}