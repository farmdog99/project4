
public class Freelist {
    
    public class FreeBlockNode {
        private int startPos;
        private int size;
        private FreeBlockNode left;
        private FreeBlockNode right;
        
        /**
         * default constructor
         */
        FreeBlockNode() {
            startPos = 0;
            size = 0;
            left = null;
            right = null;
        }
        
        /**
         * constructor
         * @param start int
         * @param s int
         * @param l FreeBlockNode
         * @param r FreeBlockNode
         */
        FreeBlockNode(int start, int s, FreeBlockNode l, FreeBlockNode r) {
            startPos = start;
            size = s;
            left = l;
            right = r;
        }

        /**
         * getter startPos
         * @return int startPos
         */
        public int getStartPos() {
            return startPos;
        }

        /**
         * getter size
         * @return int size
         */
        public int getSize() {
            return size;
        }

        /**
         * setter startPos
         * @param startPos int
         */
        public void setStartPos(int startPos) {
            this.startPos = startPos;
        }
        
        /**
         * setter size
         * @param size int
         */
        public void setSize(int size) {
            this.size = size;
        }

        /**
         * FreeBlockNode getter
         * @return right FBN
         */
        public FreeBlockNode getRight() {
            return right;
        }

        /**
         * setter right
         * @param right FBN
         */
        public void setRight(FreeBlockNode right) {
            this.right = right;
        }

        /**
         * getter left
         * @return left FBN
         */
        public FreeBlockNode getLeft() {
            return left;
        }

        /**
         * FreeBlockNode
         * @param left FBN
         */
        public void setLeft(FreeBlockNode left) {
            this.left = left;
        }
    }
}
