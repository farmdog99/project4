
/**
 * @version 1.1
 * @author alexa
 * rectangle encapsualation
 *
 */
public class Rectangle implements Comparable<Rectangle> {
    private int xCoor;
    private int yCoor;
    private int width;
    private int height;

    /**
     * Constructor
     * @param x int xcoor
     * @param y int ycoor
     * @param w int width
     * @param h int height
     */
    Rectangle(int x, int y, int w, int h)
    {
        xCoor = x;
        yCoor = y;
        width = w;
        height = h;
    }

    /**
     * @return string tostring
     * tostring override
     */
    public String toString()
    {
        return xCoor + ", " + yCoor + ", " + width + ", " + height;
    }
    /**
     * @param rect rectangle to compare
     * @return int code
     */
    @Override
    public int compareTo(Rectangle rect) {
        if (this.getXCoor() == rect.getXCoor() &&
                this.getYCoor() == rect.getYCoor() &&
                this.getHeight() == rect.getHeight() &&
                this.getWidth() == rect.getWidth()) {
            return 0; //everthing is equals intersect
        }
        else if ((this.getXCoor() + this.getWidth()) <= rect.getXCoor() ||
                (rect.getXCoor() + rect.getWidth()) <= this.getXCoor() ||
                (this.getYCoor() + this.getHeight()) <= rect.getYCoor() ||
                (rect.getYCoor() + rect.getHeight()) <= this.getYCoor())
        {
            return 1; //does not intersect
        }
        else
            return 2; //intersects
    }



    //Getters and Setters
    /**
     * getter xcoor
     * @return int xcoor
     */
    public int getXCoor() {
        return xCoor;
    }
    /**
     * getter ycoor
     * @return int ycoor
     */
    public int getYCoor() {
        return yCoor;
    }
    /**
     * getter width
     * @return int width
     */
    public int getWidth() {
        return width;
    }
    /**
     * getter height
     * @return int height
     */
    public int getHeight() {
        return height;
    }
    /**
     * set xcoor
     * @param coor int 
     */
    public void setXCoor(int coor) {
        this.xCoor = coor;
    }
    /**
     * setter ycoor
     * @param coor setter
     */
    public void setYCoor(int coor) {
        this.yCoor = coor;
    }
    /**
     * setter width
     * @param width int
     */
    public void setWidth(int width) {
        this.width = width;
    }
    /**
     * setter height
     * @param height int
     */
    public void setHeight(int height) {
        this.height = height;
    }
}
