
/* *** ODSATag: KVPair *** */
// KVPair class definition
/**
 * @version 1.1
 * @author alexa
 *
 * @param <K> key generic
 * @param <E> value generic
 */
public class KVPair<K extends Comparable<K>, E>
    implements Comparable<KVPair<K, E>> {
    /**
     * the Key element
     */
    K theKey;
    /**
     * the Value element
     */
    E theVal;

    /**
     * constructor
     * @param k generic
     * @param v generic
     */
    KVPair(K k, E v) {
        theKey = k;
        theVal = v;
    }

    /**
     * compare to function
     * @param it pass a kv pair
     * @return int denoting equivilence and intersection
     */
    public int compareTo(KVPair<K, E> it) {
        return theKey.compareTo(it.key());
    }

    /**
     * compare to K
     * @param it generic thing
     * @return int compareto
     */
    public int compareTo(K it) {
        return theKey.compareTo(it);
    }

    /**
     * key getter
     * @return key generic
     */
    public K key() {
        return theKey;
    }
    /**
     * value getter
     * @return value generic
     */
    public E value() {
        return theVal;
    }

    /**
     * overriden tostring method
     * @return string tostring
     */
    @Override
    public String toString() {
        return theKey.toString() + ", " + theVal.toString();
    }
}
/* *** ODSAendTag: KVPair *** */