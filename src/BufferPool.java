import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.BufferedWriter;
import java.nio.ByteBuffer;
/**
 * 
 * @author alexa
 * @version 1.0
 *
 */
public class BufferPool {
    private Buffer[] pool;
    private int elements;
    //private static final int REC_SIZE = 4;
    private static final int BLOCK_SIZE = 4096;
    //statistics attributes
    private int cacheHits;
    private int diskWrites;
    private int diskReads;
    private String filename;
    
    
    /**
     * constructor
     * @param buffLen int
     * @param fn String
     */
    public BufferPool(int buffLen, String fn) {
        pool = new Buffer[buffLen];
        filename = fn;
        elements = 0;
        diskReads = 0;
        diskWrites = 0;
        cacheHits = 0;
    }

    /**
     * Write bytes with bufferpool
     * @param space byte[]
     * @param size int
     * @param pos int 
     * @param file RAF
     * @throws IOException
     */
    public void writeBytes(byte[] space, int size, 
        int pos, RandomAccessFile file) 
        throws IOException {
        int temp = isPosInBufferPool(pos, file);
        if (temp != -1) {
            pool[temp].writeBuffedBytes(space, size, pos);
            cacheHits++;
            update(temp);
        }
        else {
            byte []tempBuf = new byte[BLOCK_SIZE];
            int writePos = pos - (pos % BLOCK_SIZE);
            if (pos + 4 <= file.length()) {
                file.seek(writePos);
                file.read(tempBuf, 0, BLOCK_SIZE);
            }
            Buffer buff = new Buffer(tempBuf, file, writePos);
            update();
            pool[0] = buff;
            pool[0].writeBuffedBytes(space, size, pos);
            diskReads++;
        }
        
    }
    
    /**
     * readBytes from buffer/moves around chunks of file
     * @param space byte[]
     * @param size int
     * @param pos int
     * @param file RAF
     * @throws IOException
     */
    public void readBytes(byte[] space, 
        int size, int pos, RandomAccessFile file) 
        throws IOException {
        //check if pos is in buffer pool
        int temp = isPosInBufferPool(pos, file);
        if (temp != -1) {   
            pool[temp].readBuffedBytes(space, size, pos);
            update(temp);
            cacheHits++;
        }
        else {    
            byte []tempBuf = new byte[BLOCK_SIZE];
            int readPos = pos - (pos % BLOCK_SIZE);
            file.seek(readPos);
            file.readFully(tempBuf, 0, BLOCK_SIZE);
            Buffer buff = new Buffer(tempBuf, file, readPos);
            update();
            pool[0] = buff;
            pool[0].readBuffedBytes(space, size, pos);
            diskReads++;
        }
    }
    
    /**
     * checks for position in buffer pool
     * @param pos int
     * @param file RandomAccessFile
     * @return int index found or lack there of
     */
    public int isPosInBufferPool(int pos, RandomAccessFile file) {
        if (pool.length != 0) {
            for (int i = 0; i < elements; i++) {
                if (pos >= pool[i].getPos()) {
                    if (pos < (pool[i].getPos() + BLOCK_SIZE)) {
                        if (file == pool[i].getFile()) {
                            return i;
                        }
                    }
                }
            }    
        }
        return -1;
    }
    
    /**
     * update the array for using existing buffer
     * @param index int
     */
    public void update(int index) {
        if (elements != 0) {
            Buffer temp = pool[index];
            for (int i = index; i > 0; i--) {
                pool[i] = pool[i - 1];
            }
            pool[0] = temp;
        }
    }
    
    /**
     * updates the array for adding a new buffer to the pool
     * @throws IOException 
     */
    public void update() throws IOException {
        if (elements == pool.length) {
            int last = pool.length - 1;
            if (pool[last].getDirt() == true) {
                //write to the file
                pool[last].getFile().seek(pool[last].getPos());
                pool[last].getFile().write(pool[last].getByteArr(),
                                           0, BLOCK_SIZE);
                diskWrites++;
            }
            for (int i = last; i > 0; i--) {
                pool[i] = pool[i - 1];
            } 
        }
        else {
            for (int i = elements; i > 0; i--) {
                pool[i] = pool[i - 1];
            }
            elements++;
        }
    }
    
    /**
     * flushs the buffer pool
     * @throws IOException 
     */
    public void flushBufferPool() throws IOException {
        for (int i = 0; i < elements; i++) {
            if (pool[i].getDirt()) {
                pool[i].getFile().seek(pool[i].getPos());
                pool[i].getFile().write(pool[i].getByteArr(),
                                        0, BLOCK_SIZE);
                diskWrites++;
            }            
        }
    }
    
    /**
     * writes stats of file
     * @param file File
     * @param milli long
     * @throws IOException
     */
    @SuppressWarnings("resource")
    public void writeStatistics(File file, long milli) throws IOException {
        FileWriter statsWrite = new FileWriter(file, true);
        BufferedWriter buffed = new BufferedWriter(statsWrite);
        
        buffed.write("File sorted: " + filename + "\n");
        buffed.write("Cache Hits: " + cacheHits + "\n");
        buffed.write("Disk Writes: " + diskWrites + "\n");
        buffed.write("Disk Reads: " + diskReads + "\n");
        buffed.write("Time Elapsed: " + milli + "\n");
        buffed.write("=============================\n");
        buffed.close();
    }
    
    /**
     * getter elements
     * @return int elements
     */
    public int getElements() {
        return elements;
    }
    
    /**
     * getter element access
     * @param index int
     * @return Buffer element
     */
    public Buffer elementAt(int index) {
        return pool[index];
    }
    
    /**
     * get cacheHits
     * @return int
     */
    public int getCacheHits() {
        return cacheHits;
    }

    /**
     * get diskWrites
     * @return int
     */
    public int getDiskWrites() {
        return diskWrites;
    }

    /**
     * getter diskReads
     * @return int
     */
    public int getDiskReads() {
        return diskReads;
    }
}
