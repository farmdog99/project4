

import student.TestCase;

/**
 * @author Alex Tejada
 * @version 3
 */
public class CommmandParserTest
    extends TestCase
{
    /**
     * cmdparser object to test
     */
    CmdParser cmd;

    /**
     * This method sets up the tests that follow.
     */
    public void setUp()
    {
        cmd = new CmdParser();
    }

    /**
     * test for commands
     */
    public void test1()
    {
        Rectangle rect = new Rectangle(2, 3, 2, 3);
        Rectangle rect1 = new Rectangle(3, 3, 4, 5);
        KVPair<String, Rectangle> temp = 
                new KVPair<String, Rectangle>("blahblah", rect);
        KVPair<String, Rectangle> temp1 = 
                new KVPair<String, Rectangle>("blahb", rect1);
        cmd.myList.insert(temp);
        assertEquals(1, cmd.myList.getEntries());
        cmd.myList.removeByName("blahlah");
        cmd.myList.removeByName("blahblah");
        assertEquals(0, cmd.myList.getEntries());
        cmd.myList.insert(temp1);
        cmd.myList.insert(temp);
        cmd.myList.insert(temp);
        cmd.myList.insert(temp1);
        cmd.myList.search(temp.key());
        cmd.myList.regionSearch(temp.theVal);
    }
}
