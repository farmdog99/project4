import java.io.RandomAccessFile;

/**
 * 
 * @author alexa
 * @version 1.0
 *
 */
public class Buffer {
    
    private byte[] bArr;
    private boolean dirty;
    private int position;
    private RandomAccessFile file;
    private static final int BLOCK_SIZE = 4096;
    
    /**
     * Constructor
     * @param b byte array
     * @param f raf
     * @param pos int
     */
    public Buffer(byte[] b, RandomAccessFile f, int pos) {
        dirty = false;
        bArr = b;
        file = f; 
        position = pos;
    }
    
    /**
     * setter dirty
     * @param theDirt boolean
     */
    public void setDirty(boolean theDirt) {
        dirty = theDirt;
    }
    
    /**
     * getter dirty
     * @return boolean
     */
    public boolean getDirt() {
        return dirty;
    }
    
    /**
     * getter position
     * @return int position
     */
    public int getPos() {
        return position;
    }
    
    /**
     * getter byte array
     * @return bArr byte[]
     */
    public byte[] getByteArr() {
        return bArr;
    }
    
    /**
     * get file
     * @return RandomAccessFile file
     */
    public RandomAccessFile getFile() {
        return file;
    }
    
    /**
     * read buffered bytes
     * @param space byte[] 
     * @param size int
     * @param pos int
     */
    public void readBuffedBytes(byte[] space, int size, int pos) {
        int readPos = pos % BLOCK_SIZE;
        for (int i = 0; i < size; i++) {
            space[i] = bArr[i + readPos];
        }
        //System.arraycopy(bArr, readPos, space, 0, size);
    }
    
    /**
     * write to buffered bytes[]
     * @param space byte[]
     * @param size int 
     * @param pos int
     */
    public void writeBuffedBytes(byte[] space, int size, int pos) {
        int writePos = pos % BLOCK_SIZE;
        for (int i = 0; i < size; i++) {
            bArr[i + writePos] = space[i];
        }
        //System.arraycopy(space, 0, bArr, writePos, size);
        dirty = true;
    }
}
