
/**
 * @version 1.1
 * @author alexa
 *
 */
public class Instruction {
    private String command;
    private String name;
    private int xCoor;
    private int yCoor;
    private int width;
    private int height;
    private boolean valid;

    /**
     * default contructor
     */
    Instruction()
    {
        command = null;
        name = null;
        xCoor = 0;
        yCoor = 0;
        width = 0;
        height = 0;
        valid = false;
    }

    /**
     * print an invalid instruction
     */
    void printInvalidInstruction()
    {
        String errorStr = null;
        if (command != null)
        {
            if (command.compareTo("insert") == 0) {
                errorStr = "Rectangle rejected: (" + name + 
                        ", " + xCoor + ", " + 
                        yCoor + ", " + width + ", " + height + ")";
                System.out.println(errorStr);
            }
            else if (command.compareTo("removeV") == 0 ) {
                errorStr = "Rectangle rejected: (" + xCoor + ", " + 
                        yCoor + ", " + width + ", " + height + ")";
                System.out.println(errorStr);
            }
            else if (command.compareTo("regionsearch") == 0) {
                errorStr = "Rectangle rejected: (" + xCoor + ", " +
                        yCoor + ", " + width + ", " + height + ")";
                System.out.println(errorStr);
            }
        }
    }

    /**
     * validates rect for insert/remove
     * @return boolean valid
     */
    boolean validateRectangleInsert()
    {
        if ( xCoor < 0 || yCoor < 0)
            valid = false;
        else if ( xCoor >= 1024 || yCoor >= 1024 )
            valid = false;
        else if ( width <= 0 || height <= 0)
            valid = false;
        else if ( (xCoor + width) > 1024 || (yCoor + height) > 1024 )
            valid = false;
        else
            valid = true;

        return valid;
    }

    /**
     * validates a regionsearch rect
     * @return boolean valid
     */
    boolean validateRectangleRegSearch()
    {
        if (width <= 0 || height <= 0)
            valid = false;
        else if ((xCoor + width) <= 0 || (yCoor + height) <= 0)
            valid = false;
        else if (xCoor >= 1024 || yCoor >= 1024)
            valid = false;
        else 
            valid = true;

        return valid;
    }
    /**
     * asks if inst is valid
     * @return boolean valid
     */
    boolean isValid()
    {
        return valid;
    }

    //getters
    /**
     * get command
     * @return command string
     */
    public String getCommand() {
        return command;
    }
    /**
     * get name
     * @return name string
     */
    public String getName() {
        return name;
    }
    /**
     * get x coordinate
     * @return xcoor int
     */
    public int getxCoor() {
        return xCoor;
    }
    /**
     * get ycoordinate
     * @return y coor int
     */
    public int getyCoor() {
        return yCoor;
    }
    /**
     * get width
     * @return int width
     */
    public int getWidth() {
        return width;
    }
    /**
     * get height 
     * @return an int
     */
    public int getHeight() {
        return height;
    }

    //setters
    /**
     * set command
     * @param command set string
     */
    public void setCommand(String command) {
        this.command = command;
    }
    /**
     * set name
     * @param name set string
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * set xcoor
     * @param xCoor int set
     */
    public void setxCoor(int xCoor) {
        this.xCoor = xCoor;
    }
    /**
     * set y coodinate
     * @param yCoor int set
     */
    public void setyCoor(int yCoor) {
        this.yCoor = yCoor;
    }
    /**
     * set width
     * @param width int set
     */
    public void setWidth(int width) {
        this.width = width;
    }
    /**
     * set height
     * @param height int set
     */
    public void setHeight(int height) {
        this.height = height;
    }
    /**
     * set valid
     * @param valid set boolean
     */
    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
