
import java.lang.reflect.Array;
import java.util.Random;
/**
 * 
 * @author alexa
 * @version 1.1
 *
 * @param <K> java gen
 * @param <E> java gen
 */
public class Skiplist<K extends Comparable<K>, E extends Comparable<E>> {

    /**
     * 
     * Define and Implement Skiplist node
     *
     */
    private class SkipNode {
        KVPair<K, E> element;
        SkipNode [] forward;

        /**
         * val construct
         */
        @SuppressWarnings("unchecked")
        SkipNode( KVPair<K, E> el, int level)
        {
            element = el;
            forward = (SkipNode [])
                    Array.newInstance(Skiplist.SkipNode.class, level + 1);
            for (int i = 0; i < level; i++)
                forward[i] = null;
        }

        /**
         * Skipnode constructor that checks level and builds
         * @param level is a level
         */
        @SuppressWarnings("unchecked")
        SkipNode(int level)
        {
            forward = (SkipNode [])
                    Array.newInstance(Skiplist.SkipNode.class, level + 1);
            for (int i = 0; i < level; i++)
                forward[i] = null;
        }

        /**
         * element accessor
         * @return element access
         */
        KVPair<K, E> element()
        {
            return element;
        }
    }
    //End SkipNode class definition


    private Random rnd;  //random number generator
    /**
     * head node
     */
    public SkipNode head; //head of the node
    private int level; //current level
    private int entries; //number of elements in base layer

    /**
     * Constructor for skiplist
     */
    Skiplist()
    {
        rnd = new Random();
        level = 0; 
        entries = 0;
        head = new SkipNode(0);
    }

    /** Insert a KVPair into the skiplist 
     * @param it kvPair gen
     * @return boolean check if successful
     * */
    public boolean insert(KVPair<K, E> it) {
        int newLevel = randomLevel();
        if (level < newLevel)
              adjustHead(newLevel);
        @SuppressWarnings("unchecked") // Generic array allocation
        SkipNode[] update = (SkipNode[])
        Array.newInstance(Skiplist.SkipNode.class, level + 1);
        SkipNode x = head;        // Start at header node
        for (int i = level; i >= 0; i--) { // Find insert position
            x = head;
            while ((x.forward[i] != null) && 
                  (it.compareTo((x.forward[i]).element().key()) > 0))
            x = x.forward[i];
            update[i] = x;               // Track end at level i
        }
        x = new SkipNode(it, newLevel);
        for (int i = 0; i <= newLevel; i++) {      // Splice into list
            x.forward[i] = update[i].forward[i]; // Who x points to
            update[i].forward[i] = x;            // Who y points to
        }
        entries++;                       // Increment dictionary size
        System.out.println("Rectangle inserted: (" + it.toString() + ")");
        return true;
    }

    /**
     * remove by name
     * @param rm comparable java gen
     */
    public void removeByName(Comparable<K> rm)
    {
        if (entries == 0)
        {
            System.out.println("Rectangle not removed: "
                     + rm.toString());
            return;
        }
        //KVPair<K,E> temp;
        @SuppressWarnings("unchecked") // Generic array allocation
        SkipNode[] update = (SkipNode[])
            Array.newInstance(Skiplist.SkipNode.class, level + 1);
        SkipNode x = head;        // Start at header node
        for (int i = level; i >= 0; i--) { // Find insert position
            x = head;
            while ((x.forward[i] != null) && 
                    (rm.compareTo((x.forward[i]).element().key()) > 0))
                x = x.forward[i];
            update[i] = x;               // Track end at level i
        }
        x = x.forward[0];
        if (x != null && rm.compareTo(x.element.key()) == 0)
        {
            System.out.println("Rectangle removed: (" + 
                    x.element.toString() + ")");
            for (int i = 0; i < x.forward.length; i++) {
                update[i].forward[i] = x.forward[i];
            }
            entries--;
        }
        else
        {
            System.out.println("Rectangle not removed: " +
                 rm.toString());
        }
    }

    /**
     * remove by value
     * @param e value generic
     */
    @SuppressWarnings("unchecked")
    public void removeByValue(Comparable<E> e) {
        if (entries == 0)
        {
            System.out.println("Rectangle not removed: (" +
                    e.toString() + ")");
            return;
        }
        SkipNode[] update = (SkipNode[])
                Array.newInstance(Skiplist.SkipNode.class, level + 1);
        SkipNode x  = head;
        for (int i = level; i >= 0; i--) //consult someone
        {
            x = head;
            while ((x.forward[i] != null) && 
                    (e.compareTo(x.forward[i].element.theVal) != 0)) {
                x = x.forward[i];
            }
            update[i] = x;
        }
        x = x.forward[0];
        if (x != null && e.compareTo(x.element.theVal) == 0) {
            System.out.println("Rectangle removed: (" +
                       x.element.toString() + ")");
            for (int j = 0; j < x.forward.length; j++) {
                update[j].forward[j] = x.forward[j];
            }
            entries--;

        }
        else {
            System.out.println("Rectangle not removed: (" + e.toString() + ")");
        }
    }

    /**
     * generates random level
     * @return level int
     */
    int randomLevel()
    {
        int lev;
        for (lev = 0; rnd.nextInt(2) == 0; lev++)
        {
            
        }
        return lev;
    }

    /**
     * adjust the head to match highest level
     * @param newLevel int
     */
    @SuppressWarnings("unchecked")
    public void adjustHead(int newLevel)
    {
        SkipNode temp = new SkipNode(newLevel);
        temp.forward = (SkipNode [])
                Array.newInstance(Skiplist.SkipNode.class, newLevel + 1);
        for (int i = 0; i <= level; i++)
        {
            temp.forward[i] = head.forward[i];
        }
        for (int j = level + 1; j < newLevel; j++)
        {
            temp.forward[j] = null;
        }
        head = temp;
        level = newLevel;
    }

    /**
     * dumps the skiplist
     */
    public void dump()
    {
        SkipNode temp = head;
        System.out.println("SkipList dump:");
        for (int i = 0; i < entries + 1; i++)
        {
            if (temp.element == null)
            {
                System.out.println("Node has Depth " + temp.forward.length +
                        ", Value (null)");
            }
            else
            {
                System.out.println("Node has Depth " + temp.forward.length +
                        ", Value (" + temp.element.toString() + ")");
            }
            temp = temp.forward[0];
        }
        System.out.println("SkipList size is: " + entries);
    }


    /**
     * search by key
     * @param key gen java
     * @return KVarray with search results
     */
    @SuppressWarnings("unchecked")
    public KVPair<K, E>[] search(Comparable<K> key) {
        KVPair<K, E> [] kvArray = new KVPair[entries + 1];
        int index = 0;
        int count = 0;
        SkipNode x = head;                     // Dummy header node
        for (int i = level; i >= 0; i--) {          // For each level...
            x = head;
            while ((x.forward[i] != null) &&
                    (key.compareTo(x.forward[i].element().key()) > 0)) {
                x = x.forward[i];    // Go one last step
                count++;
            }
        }
        if (x.forward != null)
            x = x.forward[0];  // Move to actual record, if it exists
        if ((x != null) && (key.compareTo(x.element().key()) == 0)) {
            kvArray[index] = x.element;
            index++;
        }
        for (int j = count; j < entries; j++) {
            if (x.forward[0] == null ) {
                return kvArray;
            }
            else if (key.compareTo(x.forward[0].element().key()) > 0) {
                return kvArray;
            }
            else if (key.compareTo(x.forward[0].element().key()) == 0) {
                x = x.forward[0];
                kvArray[index] = x.element;
                index++;
            }
        }

        return kvArray;
    }

    /**
     * prints intersections in the skip list
     */
    public void intersections()
    {
        System.out.println("Intersection Pairs:");
        SkipNode x = head;
        SkipNode y = head;
        for (int i = 0; i < entries; i++) {
            x = x.forward[0];
            y = x.forward[0];
            for (int j = (i + 1); j < entries; j++) {
                if (x.element.theVal.compareTo(y.element.theVal) == 0 || 
                        x.element.theVal.compareTo(y.element.theVal) == 2) {
                    System.out.println("(" + x.element.toString() + 
                            " | " + y.element.toString() + ")");
                    System.out.println("(" + y.element.toString() + 
                            " | " + x.element.toString() + ")");
                }
                y = y.forward[0];
            }
        }
    }

    /**
     * search by region
     * @param val gen java
     * @return kv array 
     */
    @SuppressWarnings("unchecked")
    public KVPair<K, E>[] regionSearch(Comparable<E> val) {
        System.out.println("Rectangles intersecting region (" +
                val.toString() + "):");
        KVPair<K, E> [] kvArray = new KVPair[entries + 1];
        int index = 0;
        SkipNode x = head;
        for (int i = 0; i < entries; i++) {
            x = x.forward[0];
            if (val.compareTo(x.element().theVal) == 0 ||
                    val.compareTo(x.element().theVal) == 2) {
                kvArray[index] = x.element;
                index++;
            }
        }
        return kvArray;  
    }

    /**
     * getter level
     * @return int level
     */
    public int getLevel() {
        return level;
    }

    /**
     * getter entries
     * @return int entries
     */
    public int getEntries() {
        return entries;
    }

    /**
     * setter level
     * @param aLevel int
     */
    public void setLevel(int aLevel) {
        this.level = aLevel;
    }

    /**
     * setter entries
     * @param e int entries
     */
    public void setEntries(int e) {
        this.entries = e;
    }
}
